import sys
from operator import itemgetter

def main():
	machine1 = [4, 4, 1, 5]
	machine2 = [1, 3, 2, 1]
	machine3 = [4, 3, 3, 3]
	NEH(machine1, machine2, machine3)


def priorities(m1, m2, m3):
	prior = []

	for i in range(0, len(m1)):
		prior.append((i, m1[i]+m2[i]+m3[i]))

	return prior;

def NEH(m1, m2, m3):
	prior = priorities(m1, m2, m3)
	prior = sorted(prior, key = itemgetter(1), reverse=True)

	best=[]
	
	for i in range(0, len(m1)):
		tempJob = prior[0]
		tempBest = best[:]
		bestCmax = sys.maxsize
		bestPosition = 0
		print("=================")
		print(tempBest)
		print("--------")

		for k in range(0, len(tempBest)+1):
			tempBest.insert(k, tempJob[0])
			cmax = flowshop3(m1, m2, m3, tempBest)
			print("Aktualna kolejka/cmax: "+str(tempBest)+", "+str(cmax))
			if(cmax <= bestCmax):
				bestCmax = cmax
				bestPosition = k
			tempBest.remove(tempJob[0])
		best.insert(bestPosition, tempJob[0])
		print("Wybrano perm: "+str(best))
		prior.remove(prior[0])

	print(best)
	print(flowshop3(m1, m2, m3, best))



def flowshop3(machine1time, machine2time, machine3time, queue):

	time = 1
	machine1 = -1
	machine2 = -1
	machine3 = -1
	currentTimeM1 = 0
	currentTimeM2 = 0
	currentTimeM3 = 0

	while (machine1<len(queue) or machine2<len(queue) or machine3<len(queue)):
		if currentTimeM1 == 0:
			if machine1 != len(queue)-1:
				machine1+=1
				currentTimeM1 = machine1time[queue[machine1]]
			else:
				machine1+=1

		if currentTimeM2 <= 0:
			if (machine1 > machine2 + 1 or (machine1 == machine2+1 and currentTimeM1 <= 0)):
				if machine2+1 != len(queue):
					machine2+=1
					currentTimeM2 = machine2time[queue[machine2]]
				else:
					machine2+=1

		if currentTimeM3 <= 0:
			if (machine2 > machine3+1 or (machine2 == machine3+1 and currentTimeM2 <= 0)):
				if machine3+1 != len(queue):
					machine3+=1
					currentTimeM3 = machine3time[queue[machine3]]
				else:
					machine3+=1


		currentTimeM1-=1
		currentTimeM2-=1
		currentTimeM3-=1
		time+=1


	return time-2


main();